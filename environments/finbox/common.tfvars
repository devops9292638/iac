provider_region        = "us-east-1"
region                 = "us-east-1"
vpc_state_file_address = "https://gitlab.com/api/v4/projects/50624790/terraform/state/finbox_vpc"
availability_zones     = ["us-east-1a", "us-east-1b", "us-east-1c"]
required_tags = {
  Terraform   = "true"
  Environment = "production"
  Project     = "tf-infra"
  Owner       = "DevOps"
}
