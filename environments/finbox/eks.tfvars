/*
Common Security Group: "sg-0d0c42b5cdf75d010"
public_az_subnet_ids = {
  "us-east-1a" = "subnet-0a2791d94d7ba56b8"
  "us-east-1b" = "subnet-0488b36298ada876b"
  "us-east-1c" = "subnet-0b0206d06f9420ed1"
}
db_az_subnet_ids = {
  "us-east-1a" = "subnet-0cec1f5cbc0df1b47"
  "us-east-1b" = "subnet-053ace08b297b1a08"
  "us-east-1c" = "subnet-054244f3b2e644232"
}
app_az_subnet_ids = {
  "us-east-1a" = "subnet-036aebb5a0748ec76"
  "us-east-1b" = "subnet-071e2fe4ecd157dd8"
  "us-east-1c" = "subnet-0ccffeb121bf2c270"
}
*/
env = "finbox"
cluster_name                    = "finbox-challenge"
cluster_version                 = "1.23"
cluster_endpoint_private_access = false
cluster_endpoint_public_access  = true
cloudwatch_log_group_create     = false
eks_managed_node_group_defaults = {
  disk_size                    = 100
  iam_role_attach_cni_policy   = true
  iam_role_additional_policies = [
    "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy",
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  ]

  vpc_security_group_ids = [
    "sg-0c3ddda1289e5568c"
  ]
  key_name               = "devops"
  create_launch_template = true
  launch_template_name   = "finbox"
  block_device_mappings  = {
    root = {
      device_name = "/dev/xvda"
      ebs         = {
        volume_size = 100
        volume_type = "gp3"
      }
    }
  }
}

eks_managed_node_groups = {

  ##################################
  ###      OPS NODE GROUPS       ### 
  ################################## 
  ops = {
    disk_size    = 10
    min_size     = 0
    max_size     = 1
    desired_size = 1
    labels       = {
      "managed" = "eks"
      "purpose" = "ops"
    }

    subnet_ids = ["subnet-0cec1f5cbc0df1b47", "subnet-071e2fe4ecd157dd8", "subnet-054244f3b2e644232"]

    instance_types = ["m5.xlarge"]
    capacity_type  = "ON_DEMAND"
    taints         = {
      dedicated = {
        key    = "dedicated"
        value  = "opstools"
        effect = "NO_SCHEDULE"
      }
    }
  }


  ##################################
  ###    APP NODE GROUPS         ### 
  ################################## 
  finbox_private_app = {
    disk_size    = 100
    min_size     = 0
    max_size     = 6
    desired_size = 1
    labels       = {
      "managedBy" = "eks"
      "purpose"   = "finbox-backend"
    }
    subnet_ids = ["subnet-036aebb5a0748ec76", "subnet-0c6aa30d6094599a5", "subnet-0ccffeb121bf2c270"]

    instance_types = ["m5.xlarge"]
    capacity_type  = "ON_DEMAND"
  }
}
