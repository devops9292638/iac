terraform {
  backend "http" {}
  required_version = ">= 0.13.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# terraform {
#   backend "http" {}
#   required_version = ">= 1.0.0"
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = ">= 4.9.0"
#     }
#   }
# }

provider "aws" {
  region = var.provider_region
  # assume_role {
  #   role_arn     = var.provider_role_arn
  #   session_name = "gitlab-ci"
  # }
}
