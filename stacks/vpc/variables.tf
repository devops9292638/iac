variable "region" {
  type = string
}

variable "availability_zones" {
  type = list(string)
}

variable "cidr" {
  type        = string
  description = "CIDR block for VPC"
  default     = "10.11.0.0/18"
}

variable "vpc_enabled" {
  type        = bool
  description = "Enable/Disable VPC creation. Should be enabled at the first apply"
  default     = false
}

variable "app_subnets_enabled" {
  type        = bool
  description = "Enable/Disable creation of app subnets"
  default     = true
}

variable "db_subnets_enabled" {
  type        = bool
  description = "Enable/Disable creation of db subnets"
  default     = true
}

variable "public_subnets_enabled" {
  type        = bool
  description = "Enable/Disable creation of public subnets"
  default     = true
}

variable "ipv6_enabled" {
  default     = false
  type        = bool
  description = "Enable or disable IPV6"
}

variable "max_public_subnets" {
  default     = "3"
  description = "Maximum number of subnets that should be created. The variable is used for CIDR blocks calculation"
}

variable "max_app_subnets" {
  default     = "3"
  description = "Maximum number of subnets that should be created. The variable is used for CIDR blocks calculation"
}

variable "max_db_subnets" {
  default     = "3"
  description = "Maximum number of subnets that should be created. The variable is used for CIDR blocks calculation"
}

variable "app_subnets_cidr_newbits" {
  default     = 2
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "app_subnets_cidr_netnum" {
  default     = 1
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "public_subnets_cidr_newbits" {
  default     = 2
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "public_subnets_cidr_netnum" {
  default     = 0
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "db_subnets_cidr_newbits" {
  default     = 2
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "db_subnets_cidr_netnum" {
  default     = 2
  type        = number
  description = "New bits and Netnums are used to together while using the subnetcidr function in order to generate the CIDR ranges dynamically"
}

variable "additional_tag_map_app_subnets" {
  type        = map(string)
  default     = {}
  description = <<-EOT
    Additional key-value pairs to add to each map in `tags_as_list_of_maps`. Not added to `tags` or `id`.
    This is for some rare cases where resources want additional configuration of tags
    and therefore take a list of maps with tag key, value, and additional configuration.
    EOT
}

variable "additional_tag_map_db_subnets" {
  type        = map(string)
  default     = {}
  description = <<-EOT
    Additional key-value pairs to add to each map in `tags_as_list_of_maps`. Not added to `tags` or `id`.
    This is for some rare cases where resources want additional configuration of tags
    and therefore take a list of maps with tag key, value, and additional configuration.
    EOT
}

variable "additional_tag_map_public_subnets" {
  type        = map(string)
  default     = {}
  description = <<-EOT
    Additional key-value pairs to add to each map in `tags_as_list_of_maps`. Not added to `tags` or `id`.
    This is for some rare cases where resources want additional configuration of tags
    and therefore take a list of maps with tag key, value, and additional configuration.
    EOT
}

# variable "public_network_acl_id" {
#   type        = string
#   description = "Network ACL ID that is added to the public subnets. If empty, a new ACL will be created"
#   default     = ""
# }

# variable "private_network_acl_id" {
#   type        = string
#   description = "Network ACL ID that is added to the private subnets. If empty, a new ACL will be created"
#   default     = ""
# }


variable "app_network_acl_egress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}

variable "app_network_acl_ingress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}

variable "public_network_acl_egress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}

variable "public_network_acl_ingress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}

variable "db_network_acl_egress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}

variable "db_network_acl_ingress" {
  description = "Egress network ACL rules"
  type        = list(map(string))
  default = [
    {
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
    },
  ]
}
# DHCP-OPTIONS SET

variable "enable_dhcp_options" {
  description = "Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type"
  type        = bool
  default     = true
}

variable "dhcp_options_tags" {
  description = "Additional tags for the DHCP option set (requires enable_dhcp_options set to true)"
  type        = map(string)
  default     = {}
}

variable "dhcp_options_domain_name" {
  description = "Specifies DNS name for DHCP options set (requires enable_dhcp_options set to true)"
  type        = string
  default     = ""
}

variable "dhcp_options_domain_name_servers" {
  description = "Specify a list of DNS server addresses for DHCP options set, default to AWS provided (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = ["AmazonProvidedDNS"]
}

variable "dhcp_options_ntp_servers" {
  description = "Specify a list of NTP servers for DHCP options set (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = []
}

variable "dhcp_options_netbios_name_servers" {
  description = "Specify a list of netbios servers for DHCP options set (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = []
}

variable "dhcp_options_netbios_node_type" {
  description = "Specify netbios node_type for DHCP options set (requires enable_dhcp_options set to true)"
  type        = string
  default     = ""
}

variable "common-sg-rules" {
  type        = any
  description = "List of security group rules to apply to the created security group"
}

variable "enable_s3_gateway_endpoint_app_subnet_routes" {
  description = "Creates a Route Table entry for s3 gateway endpoints in the application subnet Route tables. Should be enabled only at the first implementation if needed. If not changed after the first implementation, it would always show a diff in the statefile."
  type        = bool
  default     = false
}

variable "enable_s3_gateway_endpoint_db_subnet_routes" {
  description = "Creates a Route Table entry for s3 gateway endpoints in the application subnet Route tables. Should be enabled only at the first implementation if needed. If not changed after the first implementation, it would always show a diff in the statefile."
  type        = bool
  default     = false
}

# variable "provider_role_arn" {
#   description = "Role arn to assume"
#   type        = string
# }

variable "provider_region" {
  description = "Region to provider"
  type        = string
}


variable "ssh_key_import_enabled" {
  description = "Enable importing of ssh keys. SSH public keys must be kept in ./imports/ssh-pub"
  type        = bool
  default     = false
}


variable "ssh_public_key_file_to_import" {
  description = "A list of Keypairs to import"
  type        = list(string)
  default     = []
}
