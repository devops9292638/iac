locals {
  ipv6_enabled      = var.ipv6_enabled
  app_cidr_block    = cidrsubnet(var.cidr, var.app_subnets_cidr_newbits, var.app_subnets_cidr_netnum)
  public_cidr_block = cidrsubnet(var.cidr, var.public_subnets_cidr_newbits, var.public_subnets_cidr_netnum)
  db_cidr_block     = cidrsubnet(var.cidr, var.db_subnets_cidr_newbits, var.db_subnets_cidr_netnum)

  gateway_vpc_endpoints = {
    "s3" = {
      name = "s3"
      policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            Action = [
              "s3:*",
            ]
            Effect    = "Allow"
            Principal = "*"
            Resource  = "*"
          },
        ]
      })
    }
  }
  interface_vpc_endpoints = {}
}

################################################################################
# VPC
################################################################################
module "vpc" {
  source                           = "cloudposse/vpc/aws"
  #version                          = "2.1.0"
  version                         = "0.28.1"
  enabled                          = var.vpc_enabled
  cidr_block                       = var.cidr
  context                          = module.this.context
  assign_generated_ipv6_cidr_block = local.ipv6_enabled
}

################################################################################
# VPC Endpoints
################################################################################
module "vpc_endpoints" {
  source  = "cloudposse/vpc/aws//modules/vpc-endpoints"
  version = "0.28.1"
  #version = "2.1.0"
  vpc_id  = module.vpc.vpc_id

  gateway_vpc_endpoints   = local.gateway_vpc_endpoints
  interface_vpc_endpoints = local.interface_vpc_endpoints

  context = module.this.context
}

################################################################################
# Subnets
################################################################################
module "public_subnets" {
  source                     = "cloudposse/multi-az-subnets/aws"
  version                    = "0.15.0"
  enabled                    = var.public_subnets_enabled
  availability_zones         = var.availability_zones
  vpc_id                     = module.vpc.vpc_id
  cidr_block                 = local.public_cidr_block
  type                       = "public"
  igw_id                     = module.vpc.igw_id
  nat_gateway_enabled        = true
  ipv6_enabled               = var.ipv6_enabled
  public_network_acl_egress  = var.public_network_acl_egress
  public_network_acl_ingress = var.public_network_acl_ingress
  #ipv6_cidr_block     = local.public_ipv6_cidr_block
  max_subnets = var.max_public_subnets
  tags        = var.additional_tag_map_public_subnets

  context = module.this.context
}


module "db_subnets" {
  source                      = "cloudposse/multi-az-subnets/aws"
  version                     = "0.15.0"
  enabled                     = var.db_subnets_enabled
  availability_zones          = var.availability_zones
  vpc_id                      = module.vpc.vpc_id
  cidr_block                  = local.db_cidr_block
  type                        = "private"
  ipv6_enabled                = var.ipv6_enabled
  az_ngw_ids                  = module.public_subnets.az_ngw_ids
  private_network_acl_egress  = var.db_network_acl_egress
  private_network_acl_ingress = var.db_network_acl_ingress
  max_subnets                 = var.max_db_subnets
  stage                       = "db-ops"
  tags                        = var.additional_tag_map_db_subnets
  context                     = module.this.context
}


module "app_subnets" {
  source                      = "cloudposse/multi-az-subnets/aws"
  version                     = "0.15.0"
  enabled                     = var.app_subnets_enabled
  availability_zones          = var.availability_zones
  vpc_id                      = module.vpc.vpc_id
  cidr_block                  = local.app_cidr_block
  type                        = "private"
  ipv6_enabled                = var.ipv6_enabled
  private_network_acl_egress  = var.app_network_acl_egress
  private_network_acl_ingress = var.app_network_acl_ingress
  az_ngw_ids                  = module.public_subnets.az_ngw_ids
  max_subnets                 = var.max_app_subnets
  stage                       = "app"
  tags                        = var.additional_tag_map_app_subnets
  context                     = module.this.context
}

################################################################################
# Gateway endpoint Route Table association
################################################################################

resource "aws_vpc_endpoint_route_table_association" "s3_gateway_vpc_endpoint_app_route_table_association" {
  count           = var.enable_s3_gateway_endpoint_app_subnet_routes ? length(module.db_subnets.az_route_table_ids) : 0
  route_table_id  = element([for key, value in(module.app_subnets.az_route_table_ids) : value], count.index)
  vpc_endpoint_id = module.vpc_endpoints.gateway_vpc_endpoints[0].id
}

resource "aws_vpc_endpoint_route_table_association" "s3_gateway_vpc_endpoint_db_route_table_association" {
  count           = var.enable_s3_gateway_endpoint_db_subnet_routes ? length(module.app_subnets.az_route_table_ids) : 0
  route_table_id  = element([for key, value in(module.db_subnets.az_route_table_ids) : value], count.index)
  vpc_endpoint_id = module.vpc_endpoints.gateway_vpc_endpoints[0].id
}

################################################################################
# DHCP Options Set
################################################################################

resource "aws_vpc_dhcp_options" "this" {
  count                = var.enable_dhcp_options ? 1 : 0
  domain_name          = var.dhcp_options_domain_name
  domain_name_servers  = var.dhcp_options_domain_name_servers
  ntp_servers          = var.dhcp_options_ntp_servers
  netbios_name_servers = var.dhcp_options_netbios_name_servers
  netbios_node_type    = var.dhcp_options_netbios_node_type

  tags = module.this.tags
}

resource "aws_vpc_dhcp_options_association" "this" {
  count = var.enable_dhcp_options ? 1 : 0

  vpc_id          = module.vpc.vpc_id
  dhcp_options_id = aws_vpc_dhcp_options.this[0].id
}

################################################################################
# Common Security Group
################################################################################
module "common_security_group" {
  source     = "cloudposse/security-group/aws"
  version    = "0.4.3"
  attributes = ["common"]
  rules      = var.common-sg-rules
  vpc_id     = module.vpc.vpc_id
  context    = module.this.context
}



################################################################################
# Import SSH KEYS (Need just the public keys and not the private)
################################################################################
module "key-pair-import" {
  count               = var.ssh_key_import_enabled && length(var.ssh_public_key_file_to_import) != 0 ? length(var.ssh_public_key_file_to_import) : 0
  source              = "cloudposse/key-pair/aws"
  version             = "0.18.3"
  enabled             = var.ssh_key_import_enabled
  ssh_public_key_path = "./imports/ssh-pub"
  ssh_public_key_file = var.ssh_public_key_file_to_import[count.index]
  generate_ssh_key    = false
  label_order         = ["name"]
  name                = trim(var.ssh_public_key_file_to_import[count.index], ".pub")
  context             = module.this.context
}
