# =================== VPC & SUBNETS =================== #
vpc_enabled            = true
region                 = "us-east-1"
availability_zones     = ["us-east-1a", "us-east-1b", "us-east-1c"]
namespace              = "stage"
name                   = "metamap"
cidr                   = "10.11.0.0/18"
app_subnets_enabled    = true
db_subnets_enabled     = true
public_subnets_enabled = true
max_app_subnets        = "3"
max_db_subnets         = "3"
max_public_subnets     = "3"
label_key_case         = "title"
label_value_case       = "lower"
label_order            = ["name", "namespace", "stage", "environment", "attributes"]
ipv6_enabled           = false
additional_tag_map_public_subnets = {
  "kubernetes.io/cluster/metamap-stage" : "shared",
  "kubernetes.io/role/elb" : "1"
}
additional_tag_map_app_subnets = {
  "kubernetes.io/cluster/metamap-stage" : "shared"
}
additional_tag_map_db_subnets = {
  "kubernetes.io/cluster/metamap-stage" : "shared",
  "kubernetes.io/role/internal-elb" : "1"
}

# =================== DHCP-options-set =================== #
# enable_dhcp_options variable to be set to true (if needed) only on the first apply.
enable_dhcp_options              = true
dhcp_options_domain_name         = "metamap-stage.local"
dhcp_options_domain_name_servers = ["AmazonProvidedDNS"]
dhcp_options_ntp_servers         = ["169.254.169.123"]

# =================== S3 Gateway VPC endpoints =================== #
enable_s3_gateway_endpoint_app_subnet_routes = true
enable_s3_gateway_endpoint_db_subnet_routes  = true

# =================== Network ACL RULES =================== #
public_network_acl_egress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #OPS-VPC
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

public_network_acl_ingress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #OPS-VPC
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

db_network_acl_egress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #OPS-VPC
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

db_network_acl_ingress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0

  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #OPS-VPC
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

app_network_acl_egress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #OPS-VPC
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

app_network_acl_ingress = [
  {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
  {
    rule_no    = 101
    action     = "allow"
    cidr_block = "10.199.0.0/22" #
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    icmp_code  = 0
    icmp_type  = 0
  },
]

# Common SG-rules
common-sg-rules = [
  {
    key         = null
    type        = "ingress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.199.0.0/22"]
    description = "Allow all incoming traffic from OPS-VPC"
  },
  {
    key         = null
    type        = "ingress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.11.0.0/18"]
    description = "Allow all incoming from self.vpc"
  },
  {
    key         = null
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.199.0.0/22"]
    description = "Allow all outgoing traffic from OPS-VPC"
  },
  {
    key         = null
    type        = "egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.11.0.0/18"]
    description = "Allow all outgoing from self.vpc"
  }
]