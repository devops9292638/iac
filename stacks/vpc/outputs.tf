output "vpc_cidr" {
  value = module.vpc.vpc_cidr_block
}

output "vpc_cidr_var" {
  value = var.cidr
}

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "igw_id" {
  value       = module.vpc.igw_id
  description = "The ID of the Internet Gateway"
}

output "vpc_main_route_table_id" {
  value       = module.vpc.vpc_main_route_table_id
  description = "The ID of the main route table associated with this VPC"
}

output "vpc_default_network_acl_id" {
  value       = module.vpc.vpc_default_network_acl_id
  description = "The ID of the network ACL created by default on VPC creation"
}

output "vpc_default_security_group_id" {
  value = module.vpc.vpc_default_security_group_id
}


output "public_az_subnet_ids" {
  value = module.public_subnets.az_subnet_ids
}

output "app_az_subnet_ids" {
  value = module.app_subnets.az_subnet_ids
}

output "db_az_subnet_ids" {
  value = module.db_subnets.az_subnet_ids
}

output "public_az_subnet_arns" {
  value = module.public_subnets.az_subnet_arns
}

output "app_az_subnet_arns" {
  value = module.app_subnets.az_subnet_arns
}

output "db_az_subnet_arns" {
  value = module.db_subnets.az_subnet_arns
}


output "public_az_ngw_ids" {
  value = module.public_subnets.az_ngw_ids
}

output "app_az_ngw_ids" {
  value = module.app_subnets.az_ngw_ids
}

output "db_az_ngw_ids" {
  value = module.db_subnets.az_ngw_ids
}

output "public_az_route_table_ids" {
  value = module.public_subnets.az_route_table_ids
}

output "app_az_route_table_ids" {
  value = module.app_subnets.az_route_table_ids
}

output "db_az_route_table_ids" {
  value = module.db_subnets.az_route_table_ids
}

output "public_az_subnet_cidr_blocks" {
  value = module.public_subnets.az_subnet_cidr_blocks
}

output "app_az_subnet_cidr_blocks" {
  value = module.app_subnets.az_subnet_cidr_blocks
}

output "db_az_subnet_cidr_blocks" {
  value = module.db_subnets.az_subnet_cidr_blocks
}

output "gateway_vpc_endpoints" {
  value       = module.vpc_endpoints.gateway_vpc_endpoints
  description = "List of Gateway VPC Endpoints deployed to the VPC."
}

output "common_sg_id" {
  description = "The ID of the created Security Group"
  value       = module.common_security_group.id
}

output "common_sg_arn" {
  description = "The ARN of the created Security Group"
  value       = module.common_security_group.arn
}

output "common_sg_name" {
  description = "The name of the created Security Group"
  value       = module.common_security_group.name
}


