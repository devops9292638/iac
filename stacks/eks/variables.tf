variable "aws_auth_accounts" {
  type        = list(string)
  description = "List of account maps to add to the aws-auth configmap"
  default = [
    "454482099443",
    "990536626605",
  ]
  #  Example:
  #  default = [
  #    "123456789",
  #    "0987654321"
  #  ]
}

variable "aws_auth_roles" {
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  description = "List of role maps to add to the aws-auth configmap"
  default = [
    #    {
    #      rolearn = "arn:aws:iam::*:role/OrganizationAccountAccessRole"
    #      username = "masters"
    #      groups = ["system:masters"]
    #    }
  ]
  #  Exmaple:
  #  default = [
  #    {
  #      rolearn  = "arn:aws:iam::123456789:role/role1"
  #      username = "role1"
  #      groups   = ["system:masters"]
  #    }
  #  ]
}

variable "aws_auth_users" {
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))
  description = "List of user maps to add to the aws-auth configmap"
  default     = []
  #  Example:
  #  default = [
  #    {
  #      userarn  = "arn:aws:iam::66666666666:user/user1"
  #      username = "user1"
  #      groups   = ["system:masters"]
  #    },
  #  ]
}

variable "cluster_name" {
  type = string
  #  description = "Name of the cluster. Must be between 1-100 characters in length. Must begin with an alphanumeric character, and must only contain alphanumeric characters, dashes and underscores (^[0-9A-Za-z][A-Za-z0-9\\-]+$)."
  description = "Name of the EKS cluster. Must be between 1-100 characters in length. Must begin with an alphanumeric character, and must only contain alphanumeric characters, dashes and underscores (^[0-9A-Za-z][A-Za-z0-9\\-_]+$)."
  validation {
    condition     = can(regex("(^[0-9A-Za-z][A-Za-z0-9\\-]+$)", var.cluster_name))
    error_message = "The name is not following pattern '(^[0-9A-Za-z][A-Za-z0-9\\-]+$)'."
  }
  default = "metamap-devel"
}

variable "cloudwatch_log_group_create" {
  type = bool
  description = "Create cloudwatch log group"
  default = true
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Number of days to retain log events. Default retention - 90 days"
  default     = 90
}

variable "cluster_additional_security_group_ids" {
  type        = list(string)
  description = "List of additional, externally created security group IDs to attach to the cluster control plane"
  default     = []
}

variable "node_security_group_additional_rules" {
  description = "List of additional security group rules to add to the node security group created. Set `source_cluster_security_group = true` inside rules to set the `cluster_security_group` as source"
  type        = any
  default     = {}
}

variable "cluster_addons" {
  type        = any
  description = "Map of cluster addon configurations to enable for the cluster. Addon name can be the map keys or set with name"
  default = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
    aws-ebs-csi-driver = {
      resolve_conflicts = "OVERWRITE"
    }
  }
}

variable "cluster_enabled_log_types" {
  type        = list(string)
  description = "A list of the desired control plane logs to enable. For more information, see Amazon EKS Control Plane Logging documentation (https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html)"
  default = [
    "audit",
    "api",
    "authenticator",
    "controllerManager",
    "scheduler"
  ]
}

variable "cluster_endpoint_private_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled"
  default     = true
}

variable "cluster_endpoint_public_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled"
  default     = true
}

variable "cluster_endpoint_public_access_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint"
  default = [
    "0.0.0.0/0"
  ]
}

variable "cluster_version" {
  type        = string
  description = "Kubernetes <major>.<minor> version to use for the EKS cluster (i.e.: 1.21)"
  default     = "1.21"
}

#variable "subnet_ids" {
#  type        = list(string)
#  description = "A list of subnet IDs where the EKS cluster (ENIs) will be provisioned along with the nodes/node groups. Node groups can be deployed within a different set of subnet IDs from within the node group configuration"
#}
#
#variable "vpc_id" {
#  type        = string
#  description = "ID of the VPC where the cluster and its nodes will be provisioned"
#}

variable "tags" {
  type        = map(string)
  description = "tags which will be applied to resources"
  default = {
  }
}

variable "required_tags" {
  type        = map(string)
  description = "Required tags passed from env"
}

################################################################################
# Self Managed Node Group
################################################################################

variable "self_managed_node_groups" {
  description = "Map of self-managed node group definitions to create"
  type        = any
  default     = {}
}

variable "self_managed_node_group_defaults" {
  description = "Map of self-managed node group default configurations"
  type        = any
  default     = {}
}

################################################################################
# EKS Managed Node Group
################################################################################

variable "eks_managed_node_groups" {
  description = "Map of EKS managed node group definitions to create"
  type        = any
  default = {
    general = {
      min_size     = 1
      max_size     = 1
      desired_size = 1

      instance_types = ["t3.medium"]
      capacity_type  = "ON_DEMAND"
    }
  }
}

variable "eks_managed_node_group_defaults" {
  description = "Map of EKS managed node group default configurations"
  type        = any
  default = {
    disk_size = 10

    # We are using the IRSA created below for permissions
    # However, we have to provision a new cluster with the policy attached FIRST
    # before we can disable. Without this initial policy,
    # the VPC CNI fails to assign IPs and nodes cannot join the new cluster
    iam_role_attach_cni_policy = true
  }
}

variable "env" {
  type = string
  default = "devel"
}

## Karpenter
variable "enable_karpenter" {
  type = bool
  default = false
  description = "Enable or disable karpenter"
}