terraform {
  backend "http" {}
}

provider "aws" {
  region = var.provider_region
  alias = "virginia"
}



# Required variables
variable "provider_region" {
  type        = string
  description = "Region AWS for provider"
}


# variable "remote_state_user" {
#   type        = string
#   description = "Gitlab username to access gitlab backend"
# }
# variable "remote_state_password" {
#   type        = string
#   description = "Gitlab token to access gitlab backend"
# }

variable "vpc_state_file_address" {
  type        = string
  description = "Gitlab state file address for VPC module"
}
