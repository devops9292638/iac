data "terraform_remote_state" "vpc" {

  backend = "http"

  config = {
    address  = var.vpc_state_file_address
    # username = var.remote_state_user
    # password = var.remote_state_password
  }
}
