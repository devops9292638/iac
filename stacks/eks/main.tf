module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.10.1"

  # Cluster
  cluster_name    = var.cluster_name
  vpc_id          = local.vpc_id
  subnet_ids      = local.subnet_ids
  create          = true
  cluster_version = var.cluster_version

  #  Configmap
  #  aws_auth_accounts = var.aws_auth_accounts
  #  aws_auth_roles = var.aws_auth_roles
  #  aws_auth_users = var.aws_auth_users
  #  Determines whether to manage the aws-auth configmap. Actually is not necessary
  #  manage_aws_auth_configmap = true
  #  create_aws_auth_configmap = true

  #  Cloudwatch logs
  create_cloudwatch_log_group = var.cloudwatch_log_group_create

  #  EKS control plane
  create_cluster_security_group      = true
  cluster_security_group_description = "EKS control plane security group for ${var.cluster_name}"
  #  cluster_security_group_name = ""
  cluster_security_group_use_name_prefix = true
  cluster_security_group_tags            = local.tags

  create_iam_role = true
  #  iam_role_description = ""
  #  iam_role_name = ""
  #  iam_role_use_name_prefix = true
  iam_role_tags = local.tags

  # Nodes
  #  Determines whether to create a security group for the node groups or use the existing node_security_group_id
  create_node_security_group = true

  #  IRSA
  enable_irsa = true

  cloudwatch_log_group_retention_in_days = var.cloudwatch_log_group_retention_in_days

  cluster_additional_security_group_ids = local.cluster_additional_security_group_ids
  node_security_group_additional_rules  = var.node_security_group_additional_rules

  cluster_addons            = var.cluster_addons
  cluster_enabled_log_types = var.cluster_enabled_log_types

  cluster_endpoint_private_access      = var.cluster_endpoint_private_access
  cluster_endpoint_public_access       = var.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs

  eks_managed_node_group_defaults = var.eks_managed_node_group_defaults
  eks_managed_node_groups         = var.eks_managed_node_groups

  self_managed_node_group_defaults = var.self_managed_node_group_defaults
  self_managed_node_groups         = var.self_managed_node_groups

  cluster_tags = local.tags
  tags         = local.tags
}
