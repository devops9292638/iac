locals {
  #  Using remote state outputs
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
  cluster_additional_security_group_ids = [
    data.terraform_remote_state.vpc.outputs.common_sg_id
  ]
  env_select = {
    stage = flatten([values(data.terraform_remote_state.vpc.outputs.db_az_subnet_ids), values(data.terraform_remote_state.vpc.outputs.app_az_subnet_ids), values(data.terraform_remote_state.vpc.outputs.public_az_subnet_ids)])
    devel = flatten([values(data.terraform_remote_state.vpc.outputs.db_az_subnet_ids)])
    prod = flatten([values(data.terraform_remote_state.vpc.outputs.db_az_subnet_ids)])
  }
  subnet_ids = try(local.env_select[var.env], flatten([values(data.terraform_remote_state.vpc.outputs.db_az_subnet_ids)]))
  eks_managed_node_group_defaults = {
    disk_size = 100

    # We are using the IRSA created below for permissions
    # However, we have to provision a new cluster with the policy attached FIRST
    # before we can disable. Without this initial policy,
    # the VPC CNI fails to assign IPs and nodes cannot join the new cluster
    iam_role_attach_cni_policy = true
    iam_role_additional_policies = [
      "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy",
      aws_iam_policy.external_dns.arn,
      aws_iam_policy.aws_load_balancer_controller.arn,
    ]
    vpc_security_group_ids = [
      data.terraform_remote_state.vpc.outputs.common_sg_id
    ]
  }
  eks_managed_node_groups = {
    #    kube_system ={}
    #    monitoring = {}
    #    logging = {}
    #    gpu_public = {}
    #    gpu_private = {}
    #    general_public = {}
    ops = {
      min_size     = 3
      max_size     = 3
      desired_size = 3

      subnet_ids = values(data.terraform_remote_state.vpc.outputs.db_az_subnet_ids)

      instance_types = ["m5a.large"]
      capacity_type  = "ON_DEMAND"

      create_launch_template = false
      launch_template_name   = ""
      remote_access = {
        ec2_ssh_key = "mati"
      }
    }
    general_private = {
      min_size     = 1
      max_size     = 3
      desired_size = 1

      subnet_ids = values(data.terraform_remote_state.vpc.outputs.app_az_subnet_ids)

      instance_types = ["t3.medium", "t3.large"]
      capacity_type  = "ON_DEMAND"

      key_name = "mati"
    }
  }


  #  Using provided vars
  tags = merge(var.required_tags, var.tags)
}
