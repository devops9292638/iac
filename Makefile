PLAN_FILE=.terraform/plan.terraform
prefix=$(notdir ${CURDIR})
project_id="50624790"
ENV_NAME?=""
gitlab_state_name="${ENV_NAME}_${prefix}"
module_tfvars_path="../../environments/${ENV_NAME}/${prefix}.tfvars"
common_tfvars_path="../../environments/${ENV_NAME}/common.tfvars"

.PHONY: init get plan apply destroy list show

# Terraform
init:
	@if [[ -z "${TF_VAR_gitlab_uri}" ]]; then echo 'error: missing TF_VAR_gitlab_uri'; exit 1; fi
	@if [[ -z "${TF_VAR_gitlab_username}" ]]; then echo 'error: missing TF_VAR_gitlab_username'; exit 2; fi
	@if [[ -z "${TF_VAR_gitlab_access_token}" ]]; then echo 'error: missing TF_VAR_gitlab_access_token'; exit 3; fi
	@if [[ -z "${ENV_NAME}" ]]; then echo 'error: missing ENV_NAME'; exit 4; fi
	@rm -rf .terraform
	@echo "Reload ENV"
	@direnv reload
	@echo "gitlab = ${TF_VAR_gitlab_uri}"
	@echo "username = ${TF_VAR_gitlab_username}"
	@echo "path = ${gitlab_state_name}"
	@echo "env = ${ENV_NAME}"
	@terraform init \
	-upgrade \
		-backend-config="address=${TF_VAR_gitlab_uri}/api/v4/projects/${project_id}/terraform/state/${gitlab_state_name}" \
		-backend-config="lock_address=${TF_VAR_gitlab_uri}/api/v4/projects/${project_id}/terraform/state/${gitlab_state_name}/lock" \
		-backend-config="unlock_address=${TF_VAR_gitlab_uri}/api/v4/projects/${project_id}/terraform/state/${gitlab_state_name}/lock" \
		-backend-config="username=${TF_VAR_gitlab_username}" \
		-backend-config="password=${TF_VAR_gitlab_access_token}" \
		-backend-config="lock_method=POST" \
		-backend-config="unlock_method=DELETE"
       	-backend-config="retry_wait_min=5"

get:
	@terraform get -update ${ARGS}

plan:
	terraform plan -var-file=${common_tfvars_path} -var-file=${module_tfvars_path} -refresh=true -out=${PLAN_FILE}

plan_target:
	@if [[ -z "${TF_TARGETS}" ]]; then echo 'error: missing TF_TARGETS'; exit 5; fi
	terraform plan -var-file=${common_tfvars_path} -var-file=${module_tfvars_path} -refresh=true ${TF_TARGETS} -out=${PLAN_FILE}

apply:
	@terraform apply ${PLAN_FILE}

destroy:
	@terraform destroy -var-file=${common_tfvars_path} -var-file=${module_tfvars_path}

list:
	@terraform state list

show:
	@terraform state show ${ARGS}

export_vars:
	export TF_VAR_address="${TF_VAR_gitlab_uri}/api/v4/projects/${project_id}/terraform/state/${gitlab_state_name}"
